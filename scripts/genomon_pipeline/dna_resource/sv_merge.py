#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class SV_merge(Stage_task):

    task_name = "sv_merge"

    script_template = """#!/bin/bash

. $COMMON_SH


RUN_PYTHON {genomon_sv} merge {control_conf} {bedpe} {param_conf}

"""

    def __init__(self, qsub_option, script_dir):
        super(SV_merge, self).__init__(qsub_option, script_dir)


