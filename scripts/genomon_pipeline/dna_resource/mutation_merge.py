#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Mutation_merge(Stage_task):

    task_name = "mutation_merge"

    script_template = """#!/bin/bash

. $COMMON_SH


echo -n > {out_prefix}_genomon_mutations.result.txt || exit $?

cat_files() {{
    for i in `seq 1 1 {filecount}`
    do
        if [ _{active_annovar_flag} = _True ]
        then
            awk 'NR>1 {{print}}' {out_prefix}_mutations_candidate.${{i}}.hg19_multianno.txt >> {out_prefix}_genomon_mutations.result.txt || exit $?
        else
            cat {out_prefix}_mutations_candidate.${{i}}.hg19_multianno.txt >> {out_prefix}_genomon_mutations.result.txt || exit $?
        fi
    done
}}

RUN_COMMAND cat_files

CHECK_FILE {out_prefix}_genomon_mutations.result.txt

if [ "{result_dir}" != "" ]; then
    if [ ! -d {result_dir} ]; then
        mkdir -p {result_dir}
    fi
    cp {out_prefix}_genomon_mutations.result.txt {result_dir}
fi

"""
    def __init__(self, qsub_option, script_dir):
        super(Mutation_merge, self).__init__(qsub_option, script_dir)


