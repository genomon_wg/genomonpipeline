#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class SV_parse(Stage_task):

    task_name = "sv_parse"

    script_template = """#!/bin/bash

. $COMMON_SH


RUN_PYTHON {genomon_sv} parse {sample_conf} {param_conf}


"""

    def __init__(self, qsub_option, script_dir):
        super(SV_parse, self).__init__(qsub_option, script_dir)


