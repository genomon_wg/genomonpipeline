#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class SV_filt(Stage_task):

    task_name = "sv_filt"

    script_template = """#!/bin/sh

. $COMMON_SH


RUN_PYTHON {genomon_sv} filt {sample_conf} {param_conf}

"""

    def __init__(self, qsub_option, script_dir):
        super(SV_filt, self).__init__(qsub_option, script_dir)


