#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Mutation_call(Stage_task):

    task_name = "mutation_call"

    script_template = """#!/bin/bash

. $COMMON_SH

SGE_TASK_ID=`expr VGE_BULKJOB_ID + 1`

REGION=`sed -n "${{SGE_TASK_ID}}p" {interval_list}`
ref=${{REGION%%:*}}
ref_no1=`sed -n "/^${{ref}}\t/=" {ref_fa}.fai`
ref_no=`expr ${{ref_no1}} - 1`

echo "REGION = $REGION" 1>&2
echo "ref = $ref" 1>&2
echo "ref_no = $ref_no" 1>&2

if [ "$GENOMON_NODE_LOCAL_DIR" = "" ]; then
    output_fisher={out_prefix}.fisher_mutations.${{SGE_TASK_ID}}.txt
    output_realignment={out_prefix}.realignment_mutations.${{SGE_TASK_ID}}.txt
    output_indel={out_prefix}.indel_mutations.${{SGE_TASK_ID}}.txt
    output_breakpoint={out_prefix}.breakpoint_mutations.${{SGE_TASK_ID}}.txt
    output_simplerepeat={out_prefix}.simplerepeat_mutations.${{SGE_TASK_ID}}.txt
    output_ebfilter={out_prefix}.ebfilter_mutations.${{SGE_TASK_ID}}.txt
else
    uuid=`uuidgen`
    local_work_dir=$GENOMON_NODE_LOCAL_DIR/$uuid
    mkdir -p $local_work_dir
    output_fisher=$local_work_dir/fisher_mutations.txt
    output_realignment=$local_work_dir/realignment_mutations.txt
    output_indel=$local_work_dir/indel_mutations.txt
    output_breakpoint=$local_work_dir/breakpoint_mutations.txt
    output_simplerepeat=$local_work_dir/simplerepeat_mutations.txt
    output_ebfilter=$local_work_dir/ebfilter_mutations.txt
    export TMPDIR=$local_work_dir  # for pysam
fi

disease_bam=`echo '{disease_bam}' | sed "s/chr0/chr${{ref_no}}/"`
if [ _{control_bam} != "_None" ]; then
    control_bam=`echo '{control_bam}' | sed "s/chr0/chr${{ref_no}}/"`
fi

if [ _{control_bam_list} != "_None" ]; then
    tmp_control_bam_list="`dirname {control_bam_list}`/${{SGE_TASK_ID}}.control_panel.txt"
    while read old_file; do
        new_file="${{old_file%/*}}/chr${{ref_no}}.markdup.bam"
        echo $new_file
    done < {control_bam_list} > $tmp_control_bam_list
fi


if [ _{control_bam} = "_None" ]; then
    RUN_PYTHON {fisher} single -R ${{REGION}} -o $output_fisher --ref_fa {ref_fa} --mapping_quality {map_quality} --base_quality {base_quality}  --min_allele_freq {min_allele_freq} --post_10_q {post_10_q} --min_depth {min_depth} -1 ${{disease_bam}} --samtools_path {samtools} || exit $?

    CHECK_FILE $output_fisher

    RUN_PYTHON {mutfilter} realignment --tumor_min_mismatch {realign_min_mismatch} --normal_max_mismatch {realign_max_mismatch} --score_difference {realign_score_diff} --window_size {realign_window_size} --max_depth {realign_max_depth} --target_mutation_file $output_fisher -1 ${{disease_bam}} --output $output_realignment --ref_genome {ref_fa} --blat_path {blat} || exit $?

    CHECK_FILE $output_realignment

    RUN_PYTHON {mutfilter} simplerepeat --target_mutation_file $output_realignment --output $output_simplerepeat --simple_repeat_db {simple_repeat_db} || exit $?

    CHECK_FILE $output_simplerepeat

else
    RUN_PYTHON {fisher} comparison -t {fisher_num_procs} --pileup_margin {fisher_pileup_margin} --chunk_size {fisher_chunk_size} --max_depth {fisher_max_depth} -R ${{REGION}} -o $output_fisher --ref_fa {ref_fa} --mapping_quality {map_quality} --base_quality {base_quality}  --min_allele_freq {min_allele_freq} --max_allele_freq {max_allele_freq} --min_depth {min_depth} --fisher_value {fisher_thres} -2 ${{control_bam}} -1 ${{disease_bam}} --samtools_path {samtools} || exit $?

    CHECK_FILE $output_fisher

    RUN_PYTHON {mutfilter} realignment --process_num {realign_num_procs} --chunk_size {realign_chunk_size} --tumor_min_mismatch {realign_min_mismatch} --normal_max_mismatch {realign_max_mismatch} --score_difference {realign_score_diff} --window_size {realign_window_size} --max_depth {realign_max_depth} --target_mutation_file $output_fisher -1 ${{disease_bam}} -2 ${{control_bam}} --output $output_realignment --ref_genome {ref_fa} --blat_path {blat} || exit $?

    CHECK_FILE $output_realignment

    RUN_PYTHON {mutfilter} indel --search_length {indel_search_length} --neighbor {indel_neighbor} --base_qual {indel_base_quality} --min_depth {indel_min_depth} --min_mismatch {indel_min_mismatch} --af_thres {indel_min_allele_freq} --target_mutation_file $output_realignment -2 ${{control_bam}} --output $output_indel || exit $?

    CHECK_FILE $output_indel

    RUN_PYTHON {mutfilter} breakpoint --max_depth {bp_max_depth} --min_clip_size {bp_min_clip_size} --junc_num_thres {bp_junc_num_thres} --mapq_thres {bp_map_quality} --target_mutation_file $output_indel -2 ${{control_bam}} --output $output_breakpoint || exit $?

    CHECK_FILE $output_breakpoint

    RUN_PYTHON {mutfilter} simplerepeat --target_mutation_file $output_breakpoint --output $output_simplerepeat --simple_repeat_db {simple_repeat_db} || exit $?

    CHECK_FILE $output_simplerepeat
fi

if [ _{control_bam_list} != "_None" ]; then 
    RUN_PYTHON {EBFilter} -t {eb_num_procs} --chunk_size {eb_chunk_size} -f anno -q {eb_map_quality} -Q {eb_base_quality} $output_simplerepeat ${{disease_bam}} ${{tmp_control_bam_list}} $output_ebfilter || exit $?

    CHECK_FILE $output_ebfilter
    rm ${{tmp_control_bam_list}}
else
    RUN_COMMAND cp $output_simplerepeat $output_ebfilter
fi

if [ _{active_annovar_flag} = "_True" ];then
    RUN_PERL {annovar}/table_annovar.pl --outfile {out_prefix}_mutations_candidate.${{SGE_TASK_ID}} {table_annovar_params} $output_ebfilter {annovar}/humandb || exit $?
else
    RUN_COMMAND cp $output_ebfilter {out_prefix}_mutations_candidate.${{SGE_TASK_ID}}.hg19_multianno.txt
fi

CHECK_FILE {out_prefix}_mutations_candidate.${{SGE_TASK_ID}}.hg19_multianno.txt

if [ "$GENOMON_NODE_LOCAL_DIR" = "" ]; then
    rm -f $output_fisher \
        $output_realignment \
        $output_indel \
        $output_breakpoint \
        $output_simplerepeat \
        $output_ebfilter
else
    rm -rf $local_work_dir
fi

"""
    def __init__(self, qsub_option, script_dir):
        super(Mutation_call, self).__init__(qsub_option, script_dir)


