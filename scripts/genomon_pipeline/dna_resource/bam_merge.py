#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Bam_merge(Stage_task):

    task_name = "bam_merge"

    script_template = """#!/bin/bash

. $COMMON_SH


if [ "{result_dir}" = "" ]; then
    merged_bam={out_bam}
else
    if [ ! -d {result_dir} ]; then
        mkdir -p {result_dir}
    fi
    merged_bam="{result_dir}/`basename {out_bam}`"
fi

make_bam_list() {{
    for i in `seq 0 $(({interval_num}-1))`
    do
        echo "{output_dir}/chr$i.markdup.bam"
    done | xargs
}}

bam_list=`make_bam_list`

#
# http://www.htslib.org/doc/samtools.html
#
RUN_COMMAND samtools cat -o ${{merged_bam}} ${{bam_list}}
RUN_COMMAND bai_merge ${{bam_list}} > ${{merged_bam}}.bai

CHECK_FILE ${{merged_bam}}
CHECK_FILE ${{merged_bam}}.bai


RUN_COMMAND md5sum ${{merged_bam}} > ${{merged_bam}}.md5

CHECK_FILE ${{merged_bam}}.md5


if [ "{result_dir}" != "" ]; then
    ln -s ${{merged_bam}} {out_bam}
    ln -s ${{merged_bam}}.bai {out_bam}.bai
    ln -s ${{merged_bam}}.md5 {out_bam}.md5
fi

"""

    def __init__(self, qsub_option, script_dir):
        super(Bam_merge, self).__init__(qsub_option, script_dir)

