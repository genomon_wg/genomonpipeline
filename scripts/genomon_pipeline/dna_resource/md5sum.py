#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Md5sum(Stage_task):

    task_name = "md5sum"

    script_template = """#!/bin/bash

. $COMMON_SH

RUN_COMMAND md5sum {bam} > {md5}

CHECK_FILE {md5}

"""

    def __init__(self, qsub_option, script_dir):
        super(Md5sum, self).__init__(qsub_option, script_dir)

