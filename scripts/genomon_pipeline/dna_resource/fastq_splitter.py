#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Fastq_splitter(Stage_task):


    task_name = "fastq_splitter"

    script_template = """#!/bin/bash

. $COMMON_SH

SGE_TASK_ID=`expr VGE_BULKJOB_ID + 1`

rrs="{rrs}"

if [ $rrs = "True" ]; then
    num_lines=`RUN_COMMAND count_lines {target_dir}/1_${{SGE_TASK_ID}}.fastq` || exit $?
    echo "#lines = $num_lines" 1>&2

    let mod=num_lines%4
    if [ $mod -ne 0 ]
    then
        echo "***error: input file unexpectedly truncated" 1>&2
        exit 1
    fi

    let num_files=num_lines%{lines}==0?num_lines/{lines}:num_lines/{lines}+1
    echo "#files = $num_files" 1>&2
fi

split_file() {{
    if [ -f {target_dir}/1_${{SGE_TASK_ID}}.gz ]; then
        zcat {target_dir}/1_${{SGE_TASK_ID}}.gz | split -a 4 -d -l {lines} - {target_dir}/${{SGE_TASK_ID}}.fastq_
        status=("${{PIPESTATUS[@]}}")
        [ ${{PIPESTATUS[0]}} -ne 0 ] || echo ${{PIPESTATUS[0]}}
    else
        if [ $rrs = "True" ]; then
            round_robin_split $num_files \
              {target_dir}/1_${{SGE_TASK_ID}}.fastq \
              {target_dir}/${{SGE_TASK_ID}}.fastq_%04d || exit $?
        else
            split -a 4 -d -l {lines} \
              {target_dir}/1_${{SGE_TASK_ID}}.fastq \
              {target_dir}/${{SGE_TASK_ID}}.fastq_ || exit $?
        fi
    fi
}}


RUN_COMMAND split_file

if [ ${{SGE_TASK_ID}} -eq 1 ]; then
    if [ $rrs = "True" ]; then
        echo $num_files > {target_dir}/fastq_split_num.txt
    else
        ls -1 {target_dir}/1.fastq_* | wc -l > {target_dir}/fastq_split_num.txt
    fi
fi

"""

    def __init__(self, qsub_option, script_dir):
        super(Fastq_splitter, self).__init__(qsub_option, script_dir)


