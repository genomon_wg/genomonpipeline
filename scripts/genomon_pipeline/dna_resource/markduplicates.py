#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Markduplicates(Stage_task):

    task_name = "markduplicates"

    script_template = """#!/bin/bash

. $COMMON_SH

SGE_TASK_ID=`expr VGE_BULKJOB_ID + 1`
tmp_num=`expr ${{SGE_TASK_ID}} - 1`
num=`printf "%04d" ${{tmp_num}}`

cd {output_dir}/chr${{tmp_num}}

if [ "$GENOMON_NODE_LOCAL_DIR" = "" ]; then
    tmpfile_sort=sorted.bam.tmp
    tmpfile_markdup=markdup.bam.tmp
    bwa_sam={output_dir}/chr${{tmp_num}}/bwa.sam
    sorted_bam={output_dir}/chr${{tmp_num}}/sorted.bam
else
    #uuid=`uuidgen`
    #local_work_dir=$GENOMON_NODE_LOCAL_DIR/$uuid
    #mkdir -p $local_work_dir
    #tmpfile_sort=$local_work_dir/sorted.bam.tmp
    #tmpfile_markdup=$local_work_dir/markdup.bam.tmp
    #bwa_sam=$local_work_dir/bwa.sam
    #sorted_bam=$local_work_dir/sorted.bam
    tmpfile_sort=sorted.bam.tmp
    tmpfile_markdup=markdup.bam.tmp
    bwa_sam={output_dir}/chr${{tmp_num}}/bwa.sam
    sorted_bam={output_dir}/chr${{tmp_num}}/sorted.bam
fi

cat_files() {{
    cp ../sam_header.txt $bwa_sam
    for i in `seq 0 $(({num_files}-1))`
    do
        printf "bwa_%04d.sam\n" $i
    done | xargs cat >> $bwa_sam
}}

RUN_COMMAND cat_files

CHECK_FILE $bwa_sam

check_empty() {{
    header_size=`ls -l ../sam_header.txt | cut -d ' ' -f 5`
    #echo "header_size = $header_size" 1>&2
    sam_size=`ls -l $bwa_sam | cut -d ' ' -f 5`
    #echo "sam_size = $sam_size" 1>&2
    if [ $header_size = $sam_size ]; then
        echo 'yes'
    else
        echo 'no'
    fi
}}

empty=`check_empty`
echo "empty = $empty" 1>&2


#
# https://github.com/gt1/biobambam/blob/master/src/programs/bamsort.1
#

RUN_COMMAND {biobambam}/bamsort \
    level=1 outputthreads=2 \
    inputformat=sam \
    tmpfile=$tmpfile_sort \
    I=$bwa_sam \
    O=$sorted_bam \
    || exit $?

CHECK_FILE $sorted_bam

rm $bwa_sam

#
# https://github.com/gt1/biobambam/blob/master/src/programs/bammarkduplicates.1
#

# remedy for bammarkduplicates aborting in case bamfile contains no reads
if [ $empty = 'yes' ]; then
    num_markthreads=1
else
    num_markthreads=2
fi

RUN_COMMAND {biobambam}/bammarkduplicates \
    markthreads=$num_markthreads \
    tmpfile=$tmpfile_markdup \
    I=$sorted_bam \
    O=../chr${{tmp_num}}.markdup.bam \
    index=1 \
    indexfilename=../chr${{tmp_num}}.markdup.bam.bai \
    || exit $?

CHECK_FILE {output_dir}/chr${{tmp_num}}.markdup.bam
CHECK_FILE {output_dir}/chr${{tmp_num}}.markdup.bam.bai

cd ..

remove_chr_dir() {{
    rm -r chr${{tmp_num}}
    #if [ "$GENOMON_NODE_LOCAL_DIR" != "" ]; then
    #    rm -rf $local_work_dir
    #fi
}}

RUN_COMMAND remove_chr_dir

"""

    def __init__(self, qsub_option, script_dir):
        super(Markduplicates, self).__init__(qsub_option, script_dir)

