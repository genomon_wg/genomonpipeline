#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Bwa_align(Stage_task):

    task_name = "bwa_align"

    script_template = """#!/bin/bash

. $COMMON_SH

SGE_TASK_ID=`expr VGE_BULKJOB_ID + 1`

tmp_num=`expr ${{SGE_TASK_ID}} - 1`
num=`printf "%04d" ${{tmp_num}}`

input_fastq1={input_dir}/1.fastq_${{num}}
input_fastq2={input_dir}/2.fastq_${{num}}
if [ "$GENOMON_NODE_LOCAL_DIR" = "" ]; then
    output_sam={output_dir}/${{num}}.bwa.sam
else
    uuid=`uuidgen`
    local_work_dir=$GENOMON_NODE_LOCAL_DIR/$uuid
    mkdir -p $local_work_dir
    output_sam=$local_work_dir/bwa.sam
fi

bwa_mem() {{
    {bwa} mem {bwa_params} {ref_fa} \
        $input_fastq1 $input_fastq2 > $output_sam \
        || exit $?
}}

RUN_COMMAND bwa_mem

CHECK_FILE $input_fastq1
CHECK_FILE $input_fastq2
CHECK_FILE $output_sam

rm $input_fastq1 $input_fastq2

RUN_COMMAND scatter_sam $output_sam {output_dir}/chr%d/bwa_${{num}}.sam

if [ ${{tmp_num}} -eq 0 ]; then
    #samtools view -H $output_sam > {output_dir}/sam_header.txt
    samtools view -H $output_sam \
        | grep -v '^@PG' > {output_dir}/sam_header.txt
fi

if [ "$GENOMON_NODE_LOCAL_DIR" = "" ]; then
    rm $output_sam
else
    rm -rf $local_work_dir
fi

"""

    def __init__(self, qsub_option, script_dir):
        super(Bwa_align, self).__init__(qsub_option, script_dir)

