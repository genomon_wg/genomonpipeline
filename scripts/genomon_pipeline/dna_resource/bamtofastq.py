#! /usr/bin/env python

from genomon_pipeline.stage_task import *

class Bam2Fastq(Stage_task):

    task_name = "bam2fastq"

    script_template = """#!/bin/sh

. $COMMON_SH

RUN_COMMAND {biobambam}/bamtofastq collate=1 exclude=QCFAIL,SECONDARY,SUPPLEMENTARY filename={input_bam} F={f1_name} F2={f2_name} T={t} S={s} O={o1_name} O2={o2_name}

CHECK_FILE {f1_name}
CHECK_FILE {f2_name}
CHECK_FILE {s}
CHECK_FILE {o1_name}
CHECK_FILE {o2_name}

"""

    def __init__(self, qsub_option, script_dir):
        super(Bam2Fastq, self).__init__(qsub_option, script_dir)


