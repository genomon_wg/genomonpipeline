#!/usr/bin/env python

import cStringIO
from ruffus import *
from genomon_pipeline.config import *

from VGE.vge_conf import *


def main(args):

    ###
    # set run_conf
    run_conf.sample_conf_file = args.sample_conf_file
    run_conf.analysis_type = args.analysis_type
    run_conf.project_root = os.path.abspath(args.project_root)
    run_conf.genomon_conf_file = args.genomon_conf_file
    run_conf.task_conf_file = args.task_conf_file
    run_conf.drmaa = args.drmaa
    run_conf.stop_vge_task_error = args.stop_vge_task_error
    if args.result_dir:
        run_conf.result_dir = os.path.abspath(args.result_dir)
    ###

    ###
    # read sample list file
    sample_conf.parse_file(run_conf.sample_conf_file)
    ###

    ###
    # set and check genomon_conf config data
    fp = cStringIO.StringIO(os.path.expandvars(open(run_conf.genomon_conf_file).read()))
    genomon_conf.readfp(fp, run_conf.genomon_conf_file)
    genomon_conf_check()
    ###

    ###
    # set and check task parameter config data
    fp = cStringIO.StringIO(os.path.expandvars(open(run_conf.task_conf_file).read()))
    task_conf.readfp(fp, run_conf.task_conf_file)
    task_conf_check()
    ###

    ###
    default_vge_conf = ""
    default_vge_conf = os.getcwd() + "/vge.cfg"
    if os.path.isfile(default_vge_conf):
        pass
    elif "VGE_CONF" in os.environ:
        default_vge_conf_dir = os.environ["VGE_CONF"]
        default_vge_conf = default_vge_conf_dir + "/vge.cfg"
        if os.path.isfile(default_vge_conf):
           pass
    vge_conf.read(default_vge_conf)
    vge_conf_check()
    ###

    if run_conf.analysis_type == "dna":
        import dna_pipeline
    elif run_conf.analysis_type == "rna":
        import rna_pipeline
    else:
        raise NotImplementedError("Just DNA and RNA pipeline is prepared")

    if not (args.param_check):
        import logging
        logging.basicConfig(format='%(asctime)s:%(name)s:%(message)s')
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        pipeline_run(
                     target_tasks = args.target_tasks,
                     logger = logger,
                     verbose = args.verbose, 
                     multiprocess = args.multiprocess,
                     checksum_level = 0
                    )
