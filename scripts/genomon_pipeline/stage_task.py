#! /usr/bin/env python

import os
import sys
import datetime
from genomon_pipeline.config.run_conf import *
#import drmaa
import ruffus

from VGE.vge_task import vge_task

file_timestamp_format = "{name}_{year:0>4d}{month:0>2d}{day:0>2d}_{hour:0>2d}{min:0>2d}_{msecond:0>6d}"

class Stage_task(object):

    def __init__(self, qsub_option, script_dir):
        self.qsub_option = qsub_option
        self.script_dir = script_dir
        self.drmaa = run_conf.drmaa
        self.retry_count = 2


    def task_exec(self, arguments, max_task=0, low_priority=False):
        # Make shell script

        now = datetime.datetime.now()
        shell_script_name = file_timestamp_format.format(
                                 name=self.task_name,
                                 year=now.year,
                                 month=now.month,
                                 day=now.day,
                                 hour=now.hour,
                                 min=now.minute,
                                 msecond=now.microsecond )
        
        shell_script_full_path = "{script}/{file}.sh".format(script = self.script_dir, file = shell_script_name)
        shell_script_file = open(shell_script_full_path, 'w')
        #list = self.script_template.format(**arguments)
        #print arguments
        shell_script_file.write(self.script_template.format(**arguments))
        #shell_script_file.write("#!/bin/sh \n sleep 1\n")
        shell_script_file.close()
        os.chmod(shell_script_full_path, 0750)

        # 2016.6.16
        command = ""
        command = self.script_template.format(**arguments)

        if self.drmaa:
        
            pass
            #s = drmaa.Session()
            #s.initialize()
        
            #jt = s.createJobTemplate()
            #jt.jobName = shell_script_name
            #jt.outputPath = ':' + run_conf.project_root + '/log'
            #jt.errorPath = ':' + run_conf.project_root + '/log' 
            #jt.nativeSpecification = self.qsub_option
            #jt.remoteCommand = shell_script_full_path
            #os.chmod(shell_script_full_path, 0750)

            #returncode = 0
            #if max_task == 0:
            #    for var in range(0, (self.retry_count+1)):
            #        jobid = s.runJob(jt)
            #        returncode = 0
            #        now = datetime.datetime.now()
            #        date = now.strftime("%Y-%m-%d %H:%M")
            #        print >> sys.stderr, "Date/Time: " + date 
            #        print "Job has been submitted with id: " + jobid + " at Date/Time: " + date
            #        retval = s.wait(jobid, drmaa.Session.TIMEOUT_WAIT_FOREVER)
            #        now = datetime.datetime.now()
            #        date = now.strftime("%Y-%m-%d %H:%M")
            #        print >> sys.stderr, "Job: " + str(retval.jobId) + ' finished with status: ' + str(retval.hasExited) + ' and exit status: ' + str(retval.exitStatus) + " at Date/Time: " + date
            #        returncode = retval.exitStatus
            #        if returncode == 0: break
            #    s.deleteJobTemplate(jt)
            #    s.exit()

            #else:
            #    joblist = s.runBulkJobs(jt,1,max_task,1)
            #    all_jobids = []
            #    for var in range(0, (self.retry_count+1)):
            #        if len(all_jobids) > 0:
            #            joblist = all_jobids
            #            all_jobids = []
            #        returncode = 0
            #        now = datetime.datetime.now()
            #        date = now.strftime("%Y-%m-%d %H:%M")
            #        print >> sys.stderr, "Date/Time: " + date 
            #        print >> sys.stderr, 'Your job has been submitted with id ' + str(joblist)
            #        s.synchronize(joblist, drmaa.Session.TIMEOUT_WAIT_FOREVER, False)
            #        for curjob in joblist:
            #            print >> sys.stderr, 'Collecting job ' + curjob
            #            retval = s.wait(curjob, drmaa.Session.TIMEOUT_WAIT_FOREVER)
            #            now = datetime.datetime.now()
            #            date = now.strftime("%Y-%m-%d %H:%M")
            #            print >> sys.stderr, "Job: " + str(retval.jobId) + ' finished with status: ' + str(retval.hasExited) + ' and exit status: ' + str(retval.exitStatus) + " at Date/Time: " + date
                        
            #            if retval.exitStatus != 0:
            #                returncode = retval.exitStatus
            #                if var == self.retry_count: break
            #                jobId_list = ((retval.jobId).encode('utf-8')).split(".")
            #                taskId = int(jobId_list[1])
            #                all_jobids.extend(s.runBulkJobs(jt,taskId,taskId,1))
                       
            #        if returncode == 0: break
            #    s.deleteJobTemplate(jt)
            #    s.exit()

        else:
            if low_priority:
                return_code = vge_task(command, max_task, shell_script_name, "", "low_priority")
            else:
                return_code = vge_task(command, max_task, shell_script_name, "")

        if run_conf.stop_vge_task_error and return_code != 0:
            raise ruffus.JobSignalledBreak("NON-ZERO return code[%d]" % return_code)
