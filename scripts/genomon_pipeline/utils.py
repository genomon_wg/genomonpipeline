import os


def mkdir_if_not_exist(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)


def rm_if_exist(file):
    if os.path.exists(file):
        os.unlink(file)


def count_lines(file):
    return sum(1 for line in open(file))


def gen_dir_name_func(root, type):
    def get_dir_name(sample, with_slash=True):
        if with_slash:
            return root + "/" + type + "/" + sample + "/"
        else:
            return root + "/" + type + "/" + sample
    return get_dir_name


def gen_split_bai_func(interval_list_file, ref_fa_fai_file):
    ref_tab = {}
    for i, line in enumerate(open(ref_fa_fai_file), start=1):
        ref = line.split()[0]
        ref_tab[ref] = i

    ref_list = []
    ref0 = ''
    for line in open(interval_list_file):
        ref = line.split(':')[0]
        if ref != ref0:
            ref_list.append((ref, ref_tab[ref]))
            ref0 = ref

    def split_bai(bam):
        dir_name = os.path.dirname(bam)
        bam_real_path = os.path.realpath(bam)
        bai_real_path = os.path.realpath(bam + '.bai')
        for ref_str, ref_no in ref_list:
            split_bam = '%s/chr%d.markdup.bam' % (dir_name, ref_no - 1)
            split_bai = '%s/chr%d.markdup.bam.bai' % (dir_name, ref_no - 1)
            if not os.path.exists(split_bam):
                os.symlink(bam_real_path, split_bam)
            if not os.path.exists(split_bai):
                os.system('bai_select %d %s %s' % (ref_no, bai_real_path, split_bai))

    return split_bai
