import os
import yaml

from ruffus import *

from genomon_pipeline.config import *
from genomon_pipeline.dna_resource import *
from genomon_pipeline.utils import *

# ------------------------------------------------------------------------------
# define same utility functions and constants

get_fastq_dir = gen_dir_name_func(run_conf.project_root, "fastq")
get_bam_dir = gen_dir_name_func(run_conf.project_root, "bam")
get_mutation_dir = gen_dir_name_func(run_conf.project_root, "mutation")
get_sv_dir = gen_dir_name_func(run_conf.project_root, "sv")

log_dir = run_conf.project_root + "/log"
script_dir = run_conf.project_root + "/script"

markdup_interval_num = count_lines(genomon_conf.get("REFERENCE", "ref_fasta") + ".fai")
markdup_interval_num += 1  # for "*"

mutation_call_interval_num = count_lines(genomon_conf.get("REFERENCE", "interval_list"))

split_bai = gen_split_bai_func(genomon_conf.get("REFERENCE", "interval_list"),
                               genomon_conf.get("REFERENCE", "ref_fasta") + ".fai")

# ------------------------------------------------------------------------------

#Task0A (preparation for pipeline run)
# set task classes
bamtofastq = Bam2Fastq(task_conf.get("bam2fastq", "qsub_option"), script_dir)
fastq_splitter = Fastq_splitter(task_conf.get("split_fast", "qsub_option"), script_dir)
bwa_align = Bwa_align(task_conf.get("bwa_mem", "qsub_option"), script_dir)
markduplicates = Markduplicates(task_conf.get("markduplicates", "qsub_option"), script_dir)
mutation_call = Mutation_call(task_conf.get("mutation_call", "qsub_option"), script_dir)
mutation_merge = Mutation_merge(task_conf.get("mutation_merge", "qsub_option"), script_dir)
sv_parse = SV_parse(task_conf.get("sv_parse", "qsub_option"), script_dir)
sv_merge = SV_merge(task_conf.get("sv_merge", "qsub_option"), script_dir)
sv_filt = SV_filt(task_conf.get("sv_filt", "qsub_option"), script_dir)
bam_merge = Bam_merge(task_conf.get("markduplicates", "qsub_option"), script_dir)


#Task0B
# generate output list of 'linked fastq'
linked_fastq_list = []

for sample in sample_conf.fastq:
    if os.path.exists(get_bam_dir(sample) + '1.sorted.bam'): continue
    if os.path.exists(get_bam_dir(sample) + sample + '.markdup.bam'): continue

    fastq_prefix, ext = os.path.splitext(sample_conf.fastq[sample][0][0])
    linked_fastq_list.append([get_fastq_dir(sample) + '1_1' + ext,
                              get_fastq_dir(sample) + '1_2' + ext])


#Task0C
# generate output list of 'bam2fastq'
bam2fastq_output_list = []
for sample in sample_conf.bam_tofastq:
    if os.path.exists(get_bam_dir(sample) + '1.sorted.bam'): continue
    if os.path.exists(get_bam_dir(sample) + sample + '.markdup.bam'): continue
    bam2fastq_output_list.append([get_fastq_dir(sample) + '1_1.fastq',
                                  get_fastq_dir(sample) + '1_2.fastq'])


#Task0D
# generate input list of 'mutation call'
markdup_bam_list = []
merge_mutation_list = []
# if sample.csv [mutation_call] is not defined, then the following loop would be skipped.
for complist in sample_conf.mutation_call:
    # if mutation.result.txt already exits, then merge_mutation_list will be an empty file so that the part of task6d task will be skipped.
    if os.path.exists(get_mutation_dir(complist[0]) + complist[0] + '_genomon_mutations.result.txt'): continue
    # note : complist[0] should be "sample_tumor." otherwise, mutaiton data will be stored in "sample_normal"!
    #        So, the correct ordering expression in the inputfile, say, sample.csv [mutation_call], is needed.
    tmp_list = []
    tmp_list.append(get_bam_dir(complist[0]) + complist[0] + '.markdup.bam')
    if complist[1]:
        tmp_list.append(get_bam_dir(complist[1]) + complist[1] + '.markdup.bam')
    else:
        tmp_list.append(None)
    if complist[2]:
        tmp_list.append(get_mutation_dir('control_panel') + complist[2] + ".control_panel.txt")
    else:
        tmp_list.append(None)
    markdup_bam_list.append(tmp_list)

markdup_dummy_bam_list = []
for complist in sample_conf.mutation_call:
    if os.path.exists(get_mutation_dir(complist[0]) + complist[0] + '_genomon_mutations.result.txt'): continue
    tmp_list = []
    tmp_list.append(get_bam_dir(complist[0]) + 'chr0.markdup.bam')
    if complist[1]:
        tmp_list.append(get_bam_dir(complist[1]) + 'chr0.markdup.bam')
    else:
        tmp_list.append(None)
    if complist[2]:
        tmp_list.append(get_mutation_dir('control_panel') + complist[2] + ".control_panel.txt")
    else:
        tmp_list.append(None)
    markdup_dummy_bam_list.append(tmp_list)


#Task0E
# generate input list of 'SV parse'
parse_sv_bam_list = []
all_target_bams = []
unique_bams = []
for complist in sample_conf.sv_detection:
    tumor_sample = complist[0]
    if tumor_sample != None:
        all_target_bams.append(get_bam_dir(tumor_sample) + tumor_sample + '.markdup.bam')
    normal_sample = complist[1]
    if normal_sample != None:
        all_target_bams.append(get_bam_dir(normal_sample) + normal_sample + '.markdup.bam')
    panel_name = complist[2]
    if panel_name != None:
        for panel_sample in sample_conf.control_panel[panel_name]:
            all_target_bams.append(get_bam_dir(panel_sample) + panel_sample + '.markdup.bam')
    unique_bams = list(set(all_target_bams))

for bam in unique_bams:
    dir_name = os.path.dirname(bam)
    sample_name = os.path.basename(dir_name)
    if os.path.exists(get_sv_dir(sample_name) + sample_name + '.junction.clustered.bedpe.gz'): continue
    parse_sv_bam_list.append(bam)


#Task0F
# generate input list of 'SV merge'
unique_complist = []
merge_bedpe_list = []
for complist in sample_conf.sv_detection:
    control_panel_name = complist[2]
    if control_panel_name != None and control_panel_name not in unique_complist:
        unique_complist.append(control_panel_name)

for control_panel_name in unique_complist:
    if os.path.exists(get_sv_dir('non_matched_control_panel') + control_panel_name + '.merged.junction.control.bedpe.gz'): continue
    tmp_list = []
    tmp_list.append(get_sv_dir('config') + control_panel_name + ".control.yaml")
    for sample in sample_conf.control_panel[control_panel_name]:
        tmp_list.append(get_sv_dir(sample) + sample +".junction.clustered.bedpe.gz")
    merge_bedpe_list.append(tmp_list)


#Task0G
# generate input list of 'SV filt'
filt_bedpe_list = []
for complist in sample_conf.sv_detection:
    if os.path.exists(get_sv_dir(complist[0]) + complist[0] +'.genomonSV.result.txt'): continue
    filt_bedpe_list.append(get_sv_dir(complist[0]) + complist[0] +".junction.clustered.bedpe.gz")


#Task0H
# prepare output directories

mkdir_if_not_exist(run_conf.project_root)
mkdir_if_not_exist(run_conf.project_root + '/script')
mkdir_if_not_exist(run_conf.project_root + '/log')
mkdir_if_not_exist(run_conf.project_root + '/fastq')
mkdir_if_not_exist(run_conf.project_root + '/bam')
mkdir_if_not_exist(run_conf.project_root + '/mutation')
mkdir_if_not_exist(run_conf.project_root + '/mutation/control_panel')
mkdir_if_not_exist(run_conf.project_root + '/sv')
mkdir_if_not_exist(run_conf.project_root + '/sv/non_matched_control_panel')
mkdir_if_not_exist(run_conf.project_root + '/sv/config')

for outputfiles in (bam2fastq_output_list + linked_fastq_list):
    sample = os.path.basename(os.path.dirname(outputfiles[0]))
    fastq_dir = get_fastq_dir(sample)
    bam_dir = get_bam_dir(sample)
    mkdir_if_not_exist(fastq_dir)
    mkdir_if_not_exist(bam_dir)
    for i in range(markdup_interval_num):
        mkdir_if_not_exist(bam_dir + 'chr' + str(i))

if run_conf.result_dir: mkdir_if_not_exist(run_conf.result_dir)


#Task0I
# prepare output directory for each sample and make mutation control panel file

for complist in sample_conf.mutation_call:
    # make dir
    mutation_dir = get_mutation_dir(complist[0])
    mkdir_if_not_exist(mutation_dir)
    # make the control panel text
    control_panel_name = complist[2]
    if control_panel_name != None:
        control_panel_file = get_mutation_dir('control_panel') + control_panel_name + ".control_panel.txt"
        #                                                        ------------------ [mutation_call] third-row value.
        with open(control_panel_file,  "w") as out_handle:
            for panel_sample in sample_conf.control_panel[control_panel_name]:
                out_handle.write(get_bam_dir(panel_sample) + panel_sample + '.markdup.bam' + "\n")


#Task0J
# make SV configuration file

for complist in sample_conf.sv_detection:
    # make the control yaml file
    control_panel_name = complist[2]
    if control_panel_name != None:
        control_conf = get_sv_dir('config') + control_panel_name + ".control.yaml"
        with open(control_conf,  "w") as out_handle:
            for sample in sample_conf.control_panel[control_panel_name]:
                out_handle.write(sample + ": " + get_sv_dir(sample) + sample +".junction.clustered.bedpe.gz\n")

# --------------------------------------------------------------------------------------------------------------------------
# ******Ruffus pipeline tasks*******

#Task1B
# link the import bam to project directory 
@originate(sample_conf.bam_import.keys())
def link_import_bam(sample):

    bam = sample_conf.bam_import[sample]
    link_dir = get_bam_dir(sample)
    link_bam = link_dir + sample + '.markdup.bam'
    bam_prefix, ext = os.path.splitext(bam)

    mkdir_if_not_exist(link_dir)
    if (not os.path.exists(link_bam)) and (not os.path.exists(link_bam + '.bai')):
        os.symlink(bam, link_bam)
        if (os.path.exists(bam +'.bai')):
            os.symlink(bam +'.bai', link_bam + '.bai')
        elif (os.path.exists(bam_prefix +'.bai')):
            os.symlink(bam_prefix +'.bai', link_bam + '.bai')
    split_bai(link_bam)


#Task1A
# convert bam to fastq
@originate(bam2fastq_output_list)
def bam2fastq(outputfiles):

    output_dir = os.path.dirname(outputfiles[0])
    sample = os.path.basename(output_dir)

    arguments = {"biobambam": genomon_conf.get("SOFTWARE", "biobambam"),
                 "input_bam": sample_conf.bam_tofastq[sample],
                 "f1_name": outputfiles[0],
                 "f2_name": outputfiles[1],
                 "o1_name": output_dir + '/unmatched_first_output.txt',
                 "o2_name": output_dir + '/unmatched_second_output.txt',
                 "t": output_dir + '/temp.txt',
                 "s": output_dir + '/single_end_output.txt',
                 "log": log_dir}

    bamtofastq.task_exec(arguments)


#Task2A
# link the input fastq to project directory
@originate(linked_fastq_list, sample_conf.fastq)
def link_input_fastq(output_file, sample_list_fastq):

    fastq_dir = os.path.dirname(output_file[0])
    sample = os.path.basename(fastq_dir)
    fastq_prefix, ext = os.path.splitext(sample_list_fastq[sample][0][0])

    # Todo
    # 1. should compare the timestamps between input and linked file
    # 2. check md5sum ?

    if not os.path.exists(fastq_dir + '/1_1' + ext): os.symlink(sample_list_fastq[sample][0][0], fastq_dir + '/1_1' + ext)
    if not os.path.exists(fastq_dir + '/1_2' + ext): os.symlink(sample_list_fastq[sample][1][0], fastq_dir + '/1_2' + ext)


#Task3A
# split fastq
@subdivide([bam2fastq, link_input_fastq],
           formatter(".+/(.+).fastq"),
           "{path[0]}/*.fastq_*",
           "{path[0]}")
def split_files(input_files, output_files, target_dir):

    for oo in output_files:
        os.unlink(oo)

    rrs = False
    if task_conf.has_option("split_fast", "rrs"):
        rrs = task_conf.getboolean("split_fast", "rrs")

    arguments = {"lines": task_conf.get("split_fast", "split_fastq_line_number"),
                 "target_dir": target_dir,
                 "rrs": str(rrs),
                 "log": log_dir}

    fastq_splitter.task_exec(arguments, 2)

    #all_line_num = sum(1 for line in open(input_files[0]))

    #with open(target_dir + "/fastq_line_num.txt",  "w") as out_handle:
    #    out_handle.write(str(all_line_num))

    os.unlink(input_files[0])
    os.unlink(input_files[1])


#Task4A
#bwa
@transform(split_files,
           formatter(".+/(.+)/1.fastq_0000"),
           add_inputs("{subpath[0][2]}/fastq/{subdir[0][0]}/2.fastq_0000"),
           "{subpath[0][2]}/bam/{subdir[0][0]}/chr0/bwa_0000.sam",
           "{subdir[0][0]}",
           "{subpath[0][2]}/fastq/{subdir[0][0]}",
           "{subpath[0][2]}/bam/{subdir[0][0]}")
def map_dna_sequence(dummy_input_files, dummy_output_files,
                     sample_name, input_dir, output_dir):

    with open(input_dir + "/fastq_split_num.txt") as in_handle:
        tmp_num = in_handle.read()
        max_task = int(tmp_num)

    arguments = {"input_dir": input_dir,
                 "output_dir": output_dir,
                 "sample_name": sample_name,
                 "bwa": genomon_conf.get("SOFTWARE", "bwa"),
                 "bwa_params": task_conf.get("bwa_mem", "bwa_params"),
                 "ref_fa":genomon_conf.get("REFERENCE", "ref_fasta"),
                 "biobambam": genomon_conf.get("SOFTWARE", "biobambam"),
                 "log": log_dir}

    bwa_align.task_exec(arguments, max_task)
    #bwa_align.task_exec(arguments, max_task, low_priority=True)


#Task5A
# sort and mark duplicate reads with biobambam
@transform(map_dna_sequence,
           formatter(),
           "{subpath[0][3]}/bam/{subdir[0][1]}/chr0.markdup.bam",
           "{subpath[0][3]}/bam/{subdir[0][1]}",
           "{subpath[0][3]}/fastq/{subdir[0][1]}",
           )
def markdup(dummy_input_file, dummy_output_file, output_dir, fastq_dir):

    with open(fastq_dir + "/fastq_split_num.txt") as in_handle:
        num_files = in_handle.read().rstrip()

    arguments = {
                 "output_dir": output_dir,
                 "num_files": num_files,
                 "biobambam": genomon_conf.get("SOFTWARE", "biobambam"),
                 "log": log_dir}

    markduplicates.task_exec(arguments, max_task=markdup_interval_num)


#Task6A
# identify mutations
@follows(markdup)
@follows(link_import_bam)
@subdivide(markdup_dummy_bam_list,
           formatter(),
           "{subpath[0][2]}/mutation/{subdir[0][0]}/{subdir[0][0]}_mutations_candidate.*.hg19_multianno.txt",
           "{subdir[0][0]}",
           "{subpath[0][2]}/mutation/{subdir[0][0]}")
def identify_mutations(input_file, output_file, sample_name, output_dir):

    arguments = {
        # fisher mutation
        "fisher": genomon_conf.get("SOFTWARE", "fisher"),
        "fisher_num_procs": task_conf.get("fisher_mutation_call", "num_procs"),
        "fisher_pileup_margin": task_conf.get("fisher_mutation_call", "pileup_margin"),
        "fisher_chunk_size": task_conf.get("fisher_mutation_call", "chunk_size"),
        "fisher_max_depth": task_conf.get("fisher_mutation_call", "max_depth"),
        "map_quality": task_conf.get("fisher_mutation_call", "map_quality"),
        "base_quality": task_conf.get("fisher_mutation_call", "base_quality"),
        "min_allele_freq": task_conf.get("fisher_mutation_call", "disease_min_allele_frequency"),
        "max_allele_freq": task_conf.get("fisher_mutation_call", "control_max_allele_frequency"),
        "min_depth": task_conf.get("fisher_mutation_call", "min_depth"),
        "fisher_thres": task_conf.get("fisher_mutation_call", "fisher_thres_hold"),
        "post_10_q": task_conf.get("fisher_mutation_call", "post_10_q"),
        # realignment filter
        "mutfilter": genomon_conf.get("SOFTWARE", "mutfilter"),
        "realign_num_procs": task_conf.get("realignment_filter", "num_procs"),
        "realign_chunk_size": task_conf.get("realignment_filter", "chunk_size"),
        "realign_min_mismatch": task_conf.get("realignment_filter","disease_min_mismatch"),
        "realign_max_mismatch": task_conf.get("realignment_filter","control_max_mismatch"),
        "realign_score_diff": task_conf.get("realignment_filter","score_diff"),
        "realign_window_size": task_conf.get("realignment_filter","window_size"),
        "realign_max_depth": task_conf.get("realignment_filter","max_depth"),
        # indel filter
        "indel_search_length": task_conf.get("indel_filter","search_length"),
        "indel_neighbor": task_conf.get("indel_filter","neighbor"),
        "indel_base_quality": task_conf.get("indel_filter","base_quality"),
        "indel_min_depth": task_conf.get("indel_filter","min_depth"),
        "indel_min_mismatch": task_conf.get("indel_filter","max_mismatch"),
        "indel_min_allele_freq": task_conf.get("indel_filter","max_allele_freq"),
        # breakpoint filter
        "bp_max_depth": task_conf.get("breakpoint_filter","max_depth"),
        "bp_min_clip_size": task_conf.get("breakpoint_filter","min_clip_size"),
        "bp_junc_num_thres": task_conf.get("breakpoint_filter","junc_num_thres"),
        "bp_map_quality": task_conf.get("breakpoint_filter","map_quality"),
        # EB filter
        "EBFilter": genomon_conf.get("SOFTWARE", "ebfilter"),
        "eb_num_procs": task_conf.get("eb_filter", "num_procs"),
        "eb_chunk_size": task_conf.get("eb_filter", "chunk_size"),
        "eb_map_quality": task_conf.get("eb_filter","map_quality"),
        "eb_base_quality": task_conf.get("eb_filter","base_quality"),
        "control_bam_list": input_file[2],
        # annovar
        "active_annovar_flag": task_conf.get("annotation", "active_annovar_flag"),
        "annovar": genomon_conf.get("SOFTWARE", "annovar"),
        "table_annovar_params": task_conf.get("annotation", "table_annovar_params"),
        # commmon
        "pythonhome": genomon_conf.get("ENV", "PYTHONHOME"),
        "pythonpath": genomon_conf.get("ENV", "PYTHONPATH"),   
        "ld_library_path": genomon_conf.get("ENV", "LD_LIBRARY_PATH"),
        "ref_fa":genomon_conf.get("REFERENCE", "ref_fasta"),
        "interval_list": genomon_conf.get("REFERENCE", "interval_list"),
        "simple_repeat_db":genomon_conf.get("REFERENCE", "simple_repeat_tabix_db"),
        "disease_bam": input_file[0],
        "control_bam": input_file[1],
        "out_prefix": output_dir + '/' + sample_name,
        "samtools": genomon_conf.get("SOFTWARE", "samtools"),
        "blat": genomon_conf.get("SOFTWARE", "blat"),
        "log": log_dir}

    mutation_call.task_exec(arguments, max_task=mutation_call_interval_num)


#Task7A
# merge candidate mutations
@collate(identify_mutations,
         formatter(".+/(.+)_mutations_candidate.(.+).hg19_multianno.txt"),
         "{path[0]}/{subdir[0][0]}_genomon_mutations.result.txt",
         "{subdir[0][0]}",
         "{subpath[0][2]}/mutation/{subdir[0][0]}",
         run_conf.result_dir)
def merge_mutation(input_files, output_file, sample_name, output_dir, result_dir):

    if result_dir:
        result_dir = result_dir + '/mutation/' + sample_name
    else:
        result_dir = ""

    # if ANNOVAR is used, then its output file should include a header in the first row,
    # while Genomon EBfiltering does not add the header. So for consistency with the files,
    # the header should be removed.
    arguments = {
        "active_annovar_flag": task_conf.get("annotation", "active_annovar_flag"),
        "filecount": mutation_call_interval_num,
        "out_prefix": output_dir + '/' + sample_name,
        "result_dir": result_dir,
        "log": log_dir}

    mutation_merge.task_exec(arguments)

    for task_id in range(1,(mutation_call_interval_num + 1)):
        input_file = output_dir+'/'+sample_name+'_mutations_candidate.'+str(task_id)+'.hg19_multianno.txt'
        os.unlink(input_file)


#Task8A
# merge markduplicated bams
@transform(markdup,
           formatter(),
           "{subpath[0][2]}/bam/{subdir[0][0]}/{subdir[0][0]}.markdup.bam",
           "{subpath[0][2]}/bam/{subdir[0][0]}",
           "{subdir[0][0]}",
           run_conf.result_dir)
def merge_bam(dummy_input_file, output_file, output_dir, sample_name, result_dir):

    if result_dir:
        result_dir = result_dir + '/bam/' + sample_name
    else:
        result_dir = ""

    arguments = {
                 "output_dir": output_dir,
                 "out_bam": output_file,
                 "result_dir": result_dir,
                 "interval_num": markdup_interval_num,
                 "log": log_dir}

    bam_merge.task_exec(arguments)


#Task9A
# clean-up bams
@follows(identify_mutations)
@follows(merge_bam)
@transform(markdup,
           formatter(),
           "dummy",
           "{subpath[0][0]}",
           )
def cleanup_bam(dummy_input_file, dummy_output_file, input_dir):
    for i in range(markdup_interval_num):
        os.unlink("%s/chr%d.markdup.bam" % (input_dir, i))
        os.unlink("%s/chr%d.markdup.bam.bai" % (input_dir, i))


#Task2B
# parse SV
@follows(link_import_bam)
@follows(merge_bam)
@transform(parse_sv_bam_list,
           formatter(),
           "{subpath[0][2]}/sv/{subdir[0][0]}/{subdir[0][0]}.junction.clustered.bedpe.gz",
           "{subdir[0][0]}",
           "{subpath[0][2]}/sv/{subdir[0][0]}"
           )
def parse_sv(input_file, output_file, sample_name, dir_name):

    mkdir_if_not_exist(dir_name)
    yaml_flag = False
    sv_sampleConf = {"target": {}, "matched_control": {}, "non_matched_control_panel": {}}
    for complist in sample_conf.sv_detection:
        if sample_name == complist[0]:

            # tumor:exist, matched_normal:exist, non-matched-normal:exist
            if complist[0] != None and complist[1] != None and complist[2] != None:
                sv_sampleConf["target"]["label"] = sample_name
                sv_sampleConf["target"]["path_to_bam"] = input_file
                sv_sampleConf["target"]["path_to_output_dir"] = dir_name
                sv_sampleConf["matched_control"]["use"] = True
                sv_sampleConf["matched_control"]["path_to_bam"] = get_bam_dir(complist[1]) + complist[1] + '.markdup.bam'
                sv_sampleConf["non_matched_control_panel"]["use"] = True
                sv_sampleConf["non_matched_control_panel"]["matched_control_label"] = complist[1]
                sv_sampleConf["non_matched_control_panel"]["data_path"] = get_sv_dir('non_matched_control_panel') + complist[2] +".merged.junction.control.bedpe.gz"
                yaml_flag = True
                break

            # tumor:exist, matched_normal:exist, non-matched-normal:none
            elif complist[0] != None and complist[1] != None and complist[2] == None:
                sv_sampleConf["target"]["label"] = sample_name
                sv_sampleConf["target"]["path_to_bam"] = input_file
                sv_sampleConf["target"]["path_to_output_dir"] = dir_name
                sv_sampleConf["matched_control"]["use"] = True
                sv_sampleConf["matched_control"]["path_to_bam"] = get_bam_dir(complist[1]) + complist[1] + '.markdup.bam'
                sv_sampleConf["non_matched_control_panel"]["use"] = False
                yaml_flag = True
                break

            # tumor:exist, matched_normal:none, non-matched-normal:exist
            elif complist[0] != None and complist[1] == None and complist[2] != None:
                sv_sampleConf["target"]["label"] = sample_name
                sv_sampleConf["target"]["path_to_bam"] = input_file
                sv_sampleConf["target"]["path_to_output_dir"] = dir_name
                sv_sampleConf["matched_control"]["use"] = False
                sv_sampleConf["matched_control"]["path_to_bam"] = None
                sv_sampleConf["non_matched_control_panel"]["use"] = True
                sv_sampleConf["non_matched_control_panel"]["matched_control_label"] = None
                sv_sampleConf["non_matched_control_panel"]["data_path"] = get_sv_dir('non_matched_control_panel') + complist[2] +".merged.junction.control.bedpe.gz"
                yaml_flag = True
                break

    if not yaml_flag:
        sv_sampleConf["target"]["label"] = sample_name
        sv_sampleConf["target"]["path_to_bam"] = input_file
        sv_sampleConf["target"]["path_to_output_dir"] = dir_name
        sv_sampleConf["matched_control"]["use"] = False
        sv_sampleConf["matched_control"]["path_to_bam"] = None
        sv_sampleConf["non_matched_control_panel"]["use"] = False

    sample_yaml = get_sv_dir('config') + sample_name + ".yaml"
    hOUT = open(sample_yaml, "w")
    print >> hOUT, yaml.dump(sv_sampleConf, default_flow_style = False)
    hOUT.close()

    arguments = {"genomon_sv": genomon_conf.get("SOFTWARE", "genomon_sv"),
                 "sample_conf": sample_yaml,
                 "param_conf": task_conf.get("genomon_sv", "param_file"),
                 "pythonhome": genomon_conf.get("ENV", "PYTHONHOME"),
                 "pythonpath": genomon_conf.get("ENV", "PYTHONPATH"),
                 "ld_library_path": genomon_conf.get("ENV", "LD_LIBRARY_PATH"),
                 "log": log_dir}
    sv_parse.task_exec(arguments)


#Task3B
# merge SV
@follows(parse_sv)
@transform(merge_bedpe_list,
           formatter(".+/(?P<NAME>.+).control.yaml"),
           "{subpath[1][2]}/sv/non_matched_control_panel/{NAME[0]}.merged.junction.control.bedpe.gz")
def merge_sv(input_files,  output_file):

    arguments = {"genomon_sv": genomon_conf.get("SOFTWARE", "genomon_sv"),
                 "control_conf": input_files[0],
                 "bedpe": output_file,
                 "param_conf": task_conf.get("genomon_sv", "param_file"),
                 "pythonhome": genomon_conf.get("ENV", "PYTHONHOME"),
                 "pythonpath": genomon_conf.get("ENV", "PYTHONPATH"),
                 "ld_library_path": genomon_conf.get("ENV", "LD_LIBRARY_PATH"),
                 "log": log_dir}
    sv_merge.task_exec(arguments)


#Task4B
# filt SV
@follows(merge_sv)
@transform(filt_bedpe_list,
           formatter(),
           "{subpath[0][2]}/sv/{subdir[0][0]}/{subdir[0][0]}.genomonSV.result.txt",
           "{subdir[0][0]}",
           "{subpath[0][2]}/sv/{subdir[0][0]}")
def filt_sv(input_files,  output_file, sample_name, dir_name):

    sample_yaml = get_sv_dir('config') + sample_name + ".yaml"

    arguments = {"genomon_sv": genomon_conf.get("SOFTWARE", "genomon_sv"),
                 "sample_conf": sample_yaml,
                 "param_conf": task_conf.get("genomon_sv", "param_file"),
                 "pythonhome": genomon_conf.get("ENV", "PYTHONHOME"),
                 "pythonpath": genomon_conf.get("ENV", "PYTHONPATH"),   
                 "ld_library_path": genomon_conf.get("ENV", "LD_LIBRARY_PATH"),
                 "log": log_dir}
    sv_filt.task_exec(arguments)

